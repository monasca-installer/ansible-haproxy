# HAProxy

Install [HAProxy](http://www.haproxy.org/) TCP load balancer.

## Role main tasks

- Install HAProxy.
- Set HAProxy to autostart.
- Configure HAProxy with balancing components.
- Save HAProxy logs to separate log file.

## Role Variables

### Mandatory variables

|          Name          |                           Description                           |
|------------------------|-----------------------------------------------------------------|
| `load_balancer_host`   | IP from which HAProxy will be accessible. Example: 192.168.10.4 |
| `balancing_components` | Configuration of balancing components, see the example below.   |

### Other variables

|               Name               |     Default Value      |                                                        Description                                                         |
|----------------------------------|------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `haproxy_user`                   | "haproxy"              | The system user for HAProxy.                                                                                               |
| `haproxy_group`                  | "haproxy"              | The system group for HAProxy.                                                                                              |
| `load_balancer_wait_for_timeout` | 60                     | Timeout when checking HAProxy status by Ansible.                                                                           |
| `haproxy_conf_dir`               | "/etc/haproxy"         | Directory for HAProxy configuration file.                                                                                  |
| `haproxy_conf_default_dir`       | "/etc/default"         | Directory for file needed to allow starting HAProxy on boot.                                                               |
| `log_dir`                        | "/var/log"             | Log directory.                                                                                                             |
| `haproxy_log_file`               | "/var/log/haproxy.log" | File with all logs coming from HAProxy.                                                                                    |
| `haproxy_chroot_dir`             | "/var/lib/haproxy"     | Changes directory to `\<jail dir>` and performs a chroot() there before dropping privileges. Increases the security level. |
| `webgui_enable`                  | `false`                | Set to `true` to enable HAProxy web gui.                                                                                   |
| `webgui_port`                    | 9000                   | HAProxy will be accessible on this port.                                                                                   |
| `webgui_refresh_time`            | "10s"                  | How frequently web gui will automatically refresh.                                                                         |
| `webgui_user`                    | "haproxy"              | User to access web gui.                                                                                                    |
| `webgui_password`                | "haproxy"              | Password to access web gui.                                                                                                |
| `run_mode`                       | "Deploy"               | One of Deploy, Stop, Install, Start, or Use. The default is Deploy which will do Install, Configure, then Start.           |

### Balancing components

You need to add balancing components, that means group of nodes for which HAProxy will be acting as a load balancer/proxy.

#### Example

```yaml
balancing_components:                                     # multiple components can be defined here analogically to balancer1
  - {
      name: "balancer1",                                  # name of the component, visible in HAProxy web UI as the name of group
      load_balancing_port: 8080,                          # port on which the balancing component will be accessed
      balancing_algorithm: "roundrobin",                  # load balance algorithm
      option_check: "httpchk",                            # type of check done by HAProxy to determine service status
      nodes: ["192.168.10.10:8080","192.168.10.11:8080"], # list of back-end node IPs with ports they are listening on
      params: [                                           # OPTIONAL: when specified will add provided parameters to HAProxy `server` config
        "port 8080",
      ]
    }
  - {
      name: "balancer2",
      load_balancing_port: 1234,
      balancing_algorithm: "leastconn",
      option_check: "httpchk GET /ping",
      nodes: ["192.168.10.10:2345","192.168.10.11:2345"],
      params: [
        "port 3456",
        "inter 1000",
      ]
    }
  - {
      name: "balancer3",
      load_balancing_port: 9876,
      balancing_algorithm: "first",
      nodes: ["192.168.10.10:9876","192.168.10.11:9876"],
    }
  - {
      name: "balancer4",
      load_balancing_port: 8889,
      default_backend: "def_back",
      backends: [
        {
          name: "def_back",
          balancing_algorithm: "roundrobin",
          option_check: "tcp-check",
          nodes: ["192.168.10.10:8888","192.168.10.11:8888"],
          params: [
            "port 8888"
          ]
        },
        {
          name: "get_back",
          acl_path_beg: "/get",
          balancing_algorithm: "roundrobin",
          nodes: ["192.168.10.20:8888","192.168.10.21:8888"],
          params: [
            "port 8888"
          ]
        },
        {
          name: "post_back",
          acl_path_beg: "/post",
          balancing_algorithm: "roundrobin",
          nodes: ["192.168.10.30:8888","192.168.10.31:8888"],
          params: [
            "port 8888"
          ]
        }
      ]
    }
```

### Available load balance algorithms

Please check
[HAProxy documentation for information how they differ][balancing-algorithms]:

- `roundrobin`
- `static-rr`
- `leastconn`
- `first`
- `source`
- `uri`
- `url_param`
- `hdr(\<name>)`
- `rdp-cookie(\<name>)`

#### Web gui

When you turn on HAProxy web gui by setting `webgui_enable` to `true` then
after installation you can access it by going to
`http://\<load_balancer_host>:\<webgui_port>`

## License

Apache License, Version 2.0

[balancing-algorithms]: https://cbonte.github.io/haproxy-dconv/1.5/configuration.html#balance

## Author Information

Rafal Warzycha
